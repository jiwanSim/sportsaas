package com.sicc.sports.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportSaaSTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SportSaaSTestApplication.class, args);
	}

}
